package com.fanjj.spark.session

import com.fanjj.spark.spark.session.UserVisitSessionAnalyzeSpark.getSQLContext
import org.apache.spark.{SparkConf, SparkContext}

object test {

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setAppName("CustomSort5").setMaster("local[*]")
    // setMaster(conf)
    val sc = new SparkContext(conf)
    val sqlContext = getSQLContext(sc)
  }
}
