package com.fanjj.spark.api;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fanjj.spark.entity.TaskEntity;
import com.fanjj.spark.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskApi {

    @Autowired
    private TaskService taskService;

    public TaskEntity getTaskById(String id){

        QueryWrapper<TaskEntity> query = new QueryWrapper<>();
        query.eq("taskid",1);
       List<TaskEntity> list     = taskService.list(query);
         return list.get(0);
    }
}
