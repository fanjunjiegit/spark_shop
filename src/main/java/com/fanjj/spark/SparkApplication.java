package com.fanjj.spark;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fanjj.spark.entity.TaskEntity;
import com.fanjj.spark.services.TaskService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.List;

@SpringBootApplication

public class SparkApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context= SpringApplication.run(SparkApplication.class, args);
        TaskService taskService = context.getBean(TaskService.class);


        QueryWrapper<TaskEntity> query = new QueryWrapper<>();
        query.eq("taskid",1);
        List<TaskEntity> list     = taskService.list(query);
    }

}
