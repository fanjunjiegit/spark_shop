package com.fanjj.spark.services;

import com.baomidou.mybatisplus.extension.service.IService;
import com.fanjj.spark.entity.TaskEntity;

public interface TaskService extends IService<TaskEntity> {

    TaskEntity getTAskById(String id);
}
