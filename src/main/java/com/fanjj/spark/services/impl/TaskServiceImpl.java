package com.fanjj.spark.services.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fanjj.spark.dao.TaskDao;
import com.fanjj.spark.entity.TaskEntity;
import com.fanjj.spark.services.TaskService;
import org.springframework.stereotype.Service;

@Service("taskService")
public class TaskServiceImpl  extends ServiceImpl<TaskDao, TaskEntity> implements TaskService {


    public  TaskEntity getTAskById(String id){

        return  this.getById(id);
    }



}
